// mechanicalTurk.c
// groupNameHereAIEdition
// David Stiansy, Raiyan Zubair, Tony Dao

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Game.h"
#include "mechanicalTurk.h"

void resetLog (int lastMoveArcs, int lastMoveCampus, int lastMoveGO8,
    int lastMoveSpinoff, int lastMoveTrade);

action decideAction (Game g) {
    action nextAction;
    while (nextAction.actionCode != PASS) {
        
        int currentPlayer = getWhoseTurn(g);
        
        // Checks many resources player owns
        int numBPS = getStudents(g, currentPlayer, STUDENT_BPS);
        int numBQN = getStudents(g, currentPlayer, STUDENT_BQN);
        int numMJ = getStudents(g, currentPlayer, STUDENT_MJ);
        int numMTV = getStudents(g, currentPlayer, STUDENT_MTV);
        int numMMONEY = getStudents(g, currentPlayer, STUDENT_MMONEY);
        //int numARCs = getARCs(g, currentPlayer);
        //int numCampuses = getCampuses(g, currentPlayer);
        //int numPubs = getPublications(g, currentPlayer);
        
        // Checks game status
        int turnNum = getTurnNumber(g);
        int mostARCs = getMostARCs(g); // Who has most arcs prestige
        //int mostPubs = getMostPublications(g); // Who has most pubs
        // where your arcs/campuses are and how many you have
        
        // Game move log
        int last2ARCs = 0; // have built 2 arcs cumulatively
        int lastMoveArcs = 0;
        int lastMoveCampus = 0;
        int lastMoveGO8 = 0;
        int lastMoveTrade = 0;
        int lastMoveSpinoff = 0;
        
        // trying to get the 10 kpi bonus for having most arcs early game
        if (mostARCs == NO_ONE) {
            if (numBPS == 1 && numBQN == 1) {
                printf("Got an ARC.\n");
                nextAction.actionCode = OBTAIN_ARC;
                resetLog();
                last2ARCs++;
                lastMoveArcs++;
            }
        } else if (last2ARCs != 2) {// buy ARCs if haven't bought 2 successively yet
            if (numBPS == 1 && numBQN == 1) {
                printf("Got an ARC.\n");
                nextAction.actionCode = OBTAIN_ARC;
                resetLog();
                last2ARCs++;
                lastMoveArcs;
            }
        } else if (last2ARCs = 2) {
            // buy campus after previously building at least 2 arcs
            // if bought 2 but cannot buy campus, 
            // if turn < 60, buy arc
            if (numBPS == 1 && numBQN == 1 && numMJ == 1 && numMTV == 1) {
                printf("Got a campus.\n");
                nextAction.actionCode = BUILD_CAMPUS;
                resetLog();
                lastMoveCampus++;
                last2ARCs = 0;
            } else if(turnNum < 60) {
                if (numBPS == 1 && numBQN == 1) {
                    printf("Got an ARC.\n");
                    nextAction.actionCode = OBTAIN_ARC;
                    resetLog();
                    last2ARCs++;
                }
            }
        } else if( currentPlayer != NO_ONE ) {// if game has passed 60 turns, begin buying spinoffs
            if (turnNum > 60) {
                if (numMJ > 0 && numMTV > 0 && numMMONEY > 0) {
                    printf("Got a spinoff.\n");
                    nextAction.actionCode = START_SPINOFF;
                    resetLog();
                    lastMoveSpinoff++;
                }
            }
        } else {
            nextAction.actionCode = PASS;
            resetLog();
        }
    }
    return nextAction;
}

void resetLog (int lastMoveArcs, int lastMoveCampus, int lastMoveGO8,
    int lastMoveSpinoff, int lastMoveTrade) {
    lastMoveArcs = 0;
    lastMoveCampus = 0;
    //lastMoveGO8 = 0;
    lastMoveSpinoff = 0;
    //lastMoveTrade = 0;
}