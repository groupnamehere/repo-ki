
//pseudo code 

#include <stdio.h>
#include <stdlib.h>

#include "Game.h"

void printGameState (Game g, int player);

int main (int argc, char * argv[]) {
    
    Game g = newGame (disciplines, dice);
    
    
    while (KPIpoints < 150) {
        
        printGameState(g,player);
        //diceValue = rollDice();    //simulate throw of two dice - we will discuss how to do this in the lecture on monday

                                // or you may wish to google it and find out how you could do it!
        //diceValue += rollDice();   //roll second dice


        throwDice(g,diceValue); 

        action move = human or mechanicalTurk(AI) decide what they want to do

        //loop until player action is PASS or player wins

        while (turn is not over and game not over) {

            move = decideAction(g);

            assert that action is legal

            if (move.actionCode == START_SPINOFF) {

                //decide if outcome is patent or publication
             
            }
        }
        makeAction(g, move);

        
    }
    
    return EXIT_SUCCESS;
}

//print statistics
void printGameState (Game g, int player) {
    printf("It is player %d's turn. They have:\n", uni);
    printf("*KPI: %d\n", getKPIpoints[uni]);
    printf("*Campuses: %d\n", getCampuses[uni]);
    printf("*GO8 campuses: %d\n", getGO8s[uni]);
    printf("*ARC grants: %d\n", getARCs[uni]);
    printf("*IP patents: %d\n", getIPs[uni]);
    printf("*Publications: %d\n", getPublications[uni]);
    printf("They also have this many students of each discipline:\n");
    printf("*THDs: %d\n", getStudents[uni][STUDENT_THD]);
    printf("*BPSs: %d\n", getStudents[uni][STUDENT_BPS]);
    printf("*BQNs: %d\n", getStudents[uni][STUDENT_BQN]);
    printf("*MJs: %d\n", getStudents[uni][STUDENT_MJ]);
    printf("*MTVs: %d\n", getStudents[uni][STUDENT_MTV]);
    printf("*MMONEYSs: %d\n", getStudents[uni][STUDENT_MMONEY]);
}

// free memory