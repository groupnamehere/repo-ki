
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "Game.h"

#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
                STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
                STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

void testInitial();


int main (int argc, char *argv[]) {


	int discipline[] = DEFAULT_DISCIPLINES;
	int dice[] = DEFAULT_DICE;

}

void testInitial() {

	printf("testing newGame\n");
	int discipline[] = DEFAULT_DISCIPLINES;
	int dice[] = DEFAULT_DICE;
	Game g = newGame(discipline, dice);
	printf("test passed\n\n");

	printf("testing disposeGame\n");
	printf("test passed\n\n");

	printf("testing makeAction\n");
	printf("test passed\n\n");

	printf("testing throwDice\n");
	printf("test passed\n\n");

	printf("testing getDiscipline\n");
	assert (getDiscipline(g,0) == STUDENT_BQN);
	assert (getDiscipline(g,1) == STUDENT_MMONEY);
	assert (getDiscipline(g,2) == STUDENT_MJ);
	assert (getDiscipline(g,3) == STUDENT_MMONEY);
	assert (getDiscipline(g,4) == STUDENT_MJ);
	assert (getDiscipline(g,5) == STUDENT_BPS);
	printf("test passed\n\n");

	printf("testing getDiceValue\n");
	assert (getDiceValue(g,0) == 9);
	assert (getDiceValue(g,1) == 10);
	assert (getDiceValue(g,2) == 8);
	assert (getDiceValue(g,3) == 12);
	assert (getDiceValue(g,4) == 6);
	assert (getDiceValue(g,5) == 5);	
	printf("test passed\n\n");

	printf("testing getMostARCs\n");
	assert(getMostARCs(g)=NO_ONE);
	printf("test passed\n\n");	

	printf("testing getMostPublications\n");
	assert(getMostPublications(g)=NO_ONE);
	printf("test passed\n\n");	

	printf("testing getTurnNumber\n");
	assert(getTurnNumber(g)=-1);
	printf("test passed\n\n");	

	printf("testing getWhoseTurn\n");
	assert(getWhoseTurn(g) == NO_ONE);
	printf("test passed\n\n");	

	printf("testing getCampus\n");	
	printf("test passed\n\n");	

	printf("testing getARC\n");
	printf("test passed\n\n");	

	printf("testing isLegalAction\n");
	printf("test passed\n\n");	

	printf("testing getKPIpoints\n");
	int uni = UNI_A;
	while(uni <= UNI_C) {
    	assert(getKPIpoints(g,uni) == 20);
    	uni++;
    }
	printf("test passed\n\n");	

	printf("testing getARCs\n");
	int uni = UNI_A;
	while(uni <= UNI_C) {
    	assert(getARCs(g,uni) == 0);
    	uni++;
    }	
	printf("test passed\n\n");	

	printf("testing getGO8s\n");
	int uni = UNI_A;
	while(uni <= UNI_C) {
    	assert(getGO8s(g,uni) == 0);
    	uni++;
    }	
	printf("test passed\n\n");	

	printf("testing getCampuses\n");
	int uni = UNI_A;
	while(uni <= UNI_C) {
    	assert(getCampuses(g,uni) == 2);
    	uni++;
    }
	printf("test passed\n\n");	

	printf("testing getIPs\n");
	int uni = UNI_A;
	while(uni <= UNI_C) {
    	assert(getIPs(g,uni) == 0);
    	uni++;
    }	
	printf("test passed\n\n");	

	printf("testing getPublications\n");
	int uni = UNI_A;
	while(uni <= UNI_C) {
    	assert(getPublications(g,uni) == 0);
    	uni++;
    }	
	printf("test passed\n\n");	

	printf("testing getStudents\n");
	int uni = UNI_A;
	while(uni <= UNI_C) {
    	assert(getStudents(g,uni,STUDENT_THD) == 0);
    	assert(getStudents(g,uni,STUDENT_BPS) == 3);
    	assert(getStudents(g,uni,STUDENT_BQN) == 1);
    	assert(getStudents(g,uni,STUDENT_MJ) == 1);
    	assert(getStudents(g,uni,STUDENT_MTV) == 1);
    	assert(getStudents(g,uni,STUDENT_MMONEY) == 1);
    	uni++;
    }
	printf("test passed\n\n");	

	printf("testing getExchangeRate\n");
	printf("test passed\n\n");	
}	