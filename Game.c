// Game.c
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>

#include "Game.h"

#define NUM_DISCIPLINES 6
//memes
struct _game {
	int disciplineMap[NUM_REGIONS]; // the disciplines assigned to each tile on the map
	int diceMap[NUM_REGIONS]; //the dice values on all the tiles on the map
	int disciplineList[NUM_UNIS][NUM_DISCIPLINES]; // The amount of students in each discipline each player has
	int diceScore; // the dice and the number rolled
	int regionID; // the game map
	int KPIpoints[NUM_UNIS];
	int numArc[NUM_UNIS];
	int numGO8[NUM_UNIS];
	int numCampus[NUM_UNIS];
	int numIP[NUM_UNIS];
	int numPub[NUM_UNIS];
	int turnNumber;
};

Game newGame (int discipline[], int dice[]) {
	// I added a comment
	// I added another comment	
	Game g = malloc (sizeof(struct _game));
	assert(g != NULL);
	// This is an assert ^
	int i=0;
	while (i<NUM_REGIONS) { 
		g->disciplineMap[i] = discipline[i];
		g->diceMap[i] = dice[i];
	}
	return g;
}

void disposeGame (Game g) {
	free(g);
}

void makeAction(Game g, action a) {

}

void throwDice (Game g, int diceScore){
	srand(time(NULL));
    diceScore = ((rand() % 6) + 1) + ((rand() % 6) + 1);
}

int getDiscipline (Game g, int regionID) {
	return g->disciplineMap[regionID];
}

int getDiceValue (Game g, int regionID) {
	return g->diceMap[regionID];
}

int getMostARCs (Game g) {
	int uni = UNI_A;
	int mostARCs = 0;
	int currentARCs = 0;
	int uniMostARCs;
	while(uni < UNI_C) {
		currentARCs = g->numArc[uni];
		if (currentARCs > mostARCs) {
			mostARCs = currentARCs;
			uniMostARCs = uni;
		} 
	}
	return uniMostARCs;
}
	
int getMostPublications (Game g) {
	int uni = UNI_A;
	int mostPubs = 0;
	int currentPubs = 0;
	int uniMostPubs = NO_ONE;
	while(uni < UNI_C) {
		currentPubs = g->numPub[uni];
		if (currentPubs > mostPubs) {
			mostPubs = currentPubs;
			uniMostPubs = uni;
		} 
	}
	return uniMostPubs;
}

int getTurnNumber (Game g){
	return g->turnNumber;
}

int getWhoseTurn (Game g) {
	return ((getTurnNumber(g))%3 + 1);
}

int getCampus(Game g, path pathToVertex) {
	return 0;
}

int getARC(Game g, path pathToEdge) {
	return 0;
}

int isLegalAction (Game g, action a) {
	return TRUE;
}

int getKPIpoints (Game g, int player) {
	return g->KPIpoints[player];
}

int getARCs (Game g, int player) {
	return g->numArc[player];
}

int getGO8s (Game g, int player) {
	return g->numGO8[player];
}

int getCampuses (Game g, int player) {
	return g->numCampus[player];
}

int getIPs (Game g, int player){
	return g->numIP[player];
}

int getPublications (Game g, int player){
	return g->numPub[player];
}

int getStudents (Game g, int player, int discipline){
	return g->disciplineList[player][discipline];
}

int getExchangeRate (Game g, int player, int disciplineFrom, int disciplineTo) {
	return 0;
}

