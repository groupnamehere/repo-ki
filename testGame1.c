#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "Game.h"

#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
                STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
                STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

#define FORMULA (6*uni-(6-disciplineValue)
#define INITIAL_AMOUNT {0,3,3,1,1,1}



void testInitial(Game g);


int main (int argc, char *argv[]) {
	//initialSetUp();
	int discipline[] = DEFAULT_DISCIPLINES;
	int dice[] = DEFAULT_DICE;
	Game g = newGame(discipline, dice);
	// make array of 18 items
	// first 6 belongs to player 0, next 6 .. player 1... player 2
	// example: player 0 = 

	int disciplineList[18]={};


	printf("testing inital\n");
	testInitial(Game g);
	printf("test passed\n\n");

}

void testInitial(Game g) {

	assert(getTurnNumber(g) == (-1));

	uni = UNI_A;
	while (uni <= UNI_C) {
		assert(getKPIpoints(g,uni) == 20);
		assert(getARCs(g,uni) == 0);
		assert(getGO8s(g,uni) == 0);
		assert(getCampuses(g,uni) == 2);
		assert(getIPs(g,uni) == 0);
		assert(getPublications(g,uni) == 0);

		int disciplineCounter = 0;
		while(disciplineCounter < 6){
			int checkDiscipline[]=INITIAL_AMOUNT;
			assert(getStudents(g,uni,disciplineList[FORMULA]) == checkDiscipline[disciplineCounter]);
			disciplineCounter++;
		}
		uni++;
	}
	throwDice(g,diceScore)	
	int counter=0;
	while(counter < 19) {
		if (dice[counter]) == diceScore) {
			

		}
		counter++;
	}
	assert(getTurnNumber(g) == (0) );

	makeAction()
	disposeGame(Game g);
}	

void 


