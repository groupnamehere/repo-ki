/*
 *  Mr Pass.  Brain the size of a planet!
 *
 *  Proundly Created by Richard Buckland
 *  Share Freely Creative Commons SA-BY-NC 3.0. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Game.h"
#include "mechanicalTurk.h"

action decideAction (Game g) {

   action nextAction;
   nextAction.actionCode = PASS;

   int currentPlayer = getWhoseTurn(g); 
  
   if( currentPlayer != NO_ONE ) {
	   int numMJ = getStudents(g, currentPlayer, STUDENT_MJ);
	   int numMTV = getStudents(g, currentPlayer, STUDENT_MTV);
	   int numMMONEY = getStudents(g, currentPlayer, STUDENT_MMONEY);
	   if (numMJ > 0 && numMTV > 0 && numMMONEY > 0) {
	   	nextAction.actionCode = START_SPINOFF;
	   }
   }
   	
   return nextAction;
}