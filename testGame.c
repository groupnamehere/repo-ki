#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "Game.h"

#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
                STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
                STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

#define FORMULA (6*uni-(6-disciplineValue)
#define INITIAL_AMOUNT {0,3,3,1,1,1}



void testInitial(int disciplineList, int dice);


int main (int argc, char *argv[]) {
	//initialSetUp();
	int discipline[] = DEFAULT_DISCIPLINES;
	int dice[] = DEFAULT_DICE;
	Game g = newGame(discipline, dice);
	// make array of 18 items
	// first 6 belongs to player 0, next 6 .. player 1... player 2
	// example: player 0 = 

	int disciplineList[18]={};


	printf("testing newGame\n");
	testInitial(Game g);
	printf("test passed\n\n");

}

void testInitial(Game g) {

	// printf("testing disposeGame\n");
	// printf("test passed\n\n");

	printf("testing makeAction\n");
	printf("test passed\n\n");

	printf("testing throwDice... ");
	printf("test passed\n\n");

	printf("testing getDiscipline... ");
	int disciplinePosition = STUDENT_THD;
	while (disciplinePosition <= STUDENT_MMONEY){
		assert (getDiscipline(g,disciplinePosition) == disciplinePosition);
		disciplinePosition++;
	}
	printf("PASSED\n");


	
	printf("testing getDiceValue... ");
	int diceCounter =		 0;
	while (dice <= (sizeof(dice)) ) {
		assert (getDiceValue(g,diceCounter) == dice[diceCounter]);
		diceCounter++;
	}	
	printf("PASSED\n\n");

	printf("testing getMostARCs\n");
	assert(getMostARCs(g) == NO_ONE);
	printf("test passed\n\n");	

	printf("testing getMostPublications\n");
	assert(getMostPublications(g) == NO_ONE);
	printf("test passed\n\n");	

	printf("testing getTurnNumber... ");
	assert(getTurnNumber(g) == (-1) );
	printf("PASSED\n");	

	printf("testing getWhoseTurn\n");
	assert(getWhoseTurn(g) == NO_ONE);
	printf("test passed\n\n");	 

	printf("testing getCampus\n");	
	printf("test passed\n\n");	

	printf("testing getARC\n");
	printf("test passed\n\n");	

	printf("testing isLegalAction\n");

	printf("test passed\n\n");	



	printf("Testing:	-> getKPIpoints\n");
	uni = uni_A;
	while (uni <= UNI_C) {
		makeAction(g, action BUILD_GO8)
		assert(getKPIpoints(g,uni) == 20);
	}
	printf("			-> getARCs\n");
	uni = uni_A;
	while (uni <= UNI_C) {
		makeAction(g, action BUILD_GO8)
		assert(getKPIpoints(g,uni) == 20);
	}	
	printf("			-> getGO8s\n");
	printf("			-> getCampuses\n");
	printf("			-> getIPs\n");
	printf("			-> getPublications\n");
	printf("			-> getStudents\n")

	uni = uni_A;
	while (uni <= UNI_C) {
		assert(getKPIpoints(g,uni) == 20);
		assert(getARCs(g,uni) == 0);
		assert(getGO8s(g,uni) == 0);
		assert(getCampuses(g,uni) == 2);
		assert(getIPs(g,uni) == 0);
		assert(getPublications(g,uni) == 0);

		int disciplineCounter = 0;
		while(disciplineCounter < 6){
			int checkDiscipline[]=INITIAL_AMOUNT;
			assert(getStudents(g,uni,disciplineList[FORMULA]) == checkDiscipline[disciplineCounter]);
			disciplineCounter++;
		}


		uni++;
	}
	printf("test passed\n\n");	


	/*
	printf("testing getStudents\n");
	printf("test passed\n\n");	
	*/

	printf("testing getExchangeRate\n");
	printf("test passed\n\n");	
}	


